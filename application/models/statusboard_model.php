<?php

class Statusboard_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getStudioBDay()
    {

	    $sid = 'studio_bdays';
		$cid = 'WTNH';
		$crss = './cache/cached_'.$sid.'_'.$cid.'.xml';

		$ctime = '1200';

		// Check for the our local non-cache_lite file
		if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
		{
			//echo "Create bday cache";
		    //MYSQL
			// Compare the dates based on what day of the year it is. (1 - 365)
			$sql = "select * from employees where Dayofyear(Birth_Date) >= DayofYear(NOW()) and Archive = 0 order by Dayofyear(Birth_Date), year(Birth_Date) ASC Limit 7";
	        //$query = $this->db->get('employees', 10);

	        //$this->db->cache_on();
	        $results = $this->db->query($sql);

			$rows = array();
			foreach ($results->result() as $row){
				$rows[] = $row;
			}

			$content = json_encode($rows);

	        $fp = fopen('./cache/cached_'.$sid.'_'.$cid.'.xml', 'w+');
			fwrite($fp,$content);
			fclose($fp);

	        return json_decode($content);

        } else {

			//echo "Reading bday cache";
			$content = file_get_contents($crss);
			return json_decode($content);

        }
    }

	function getStudioAnn()
    {

	    $sid = 'studio_anndays';
		$cid = 'WTNH';
		$crss = './cache/cached_'.$sid.'_'.$cid.'.xml';

		$ctime = '1200';

		// Check for the our local non-cache_lite file
		if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
		{
			//echo "Create ann cache";
		    // Compare the dates based on what day of the year it is. (1 - 365)
			// includes TIMESTAMPDIFF to calculate number of years a person has worked at WTNH
			$sql = "select *,TIMESTAMPDIFF(YEAR, Hire_Date, CURDATE()) AS numberofyears from employees where Dayofyear(Hire_Date) >= DayofYear(NOW()) and Archive = 0 order by Dayofyear(Hire_Date) ASC Limit 7";

			//$this->db->cache_on();
	        $results = $this->db->query($sql);

	        $rows = array();
			foreach ($results->result() as $row){
				$rows[] = $row;
			}

			$content = json_encode($rows);

	        $fp = fopen('./cache/cached_'.$sid.'_'.$cid.'.xml', 'w+');
			fwrite($fp,json_encode($rows));
			fclose($fp);

	        return json_decode($content);

        } else {

			//echo "Reading ann cache";

			$content = file_get_contents($crss);
			return json_decode($content);
        }
	}
}