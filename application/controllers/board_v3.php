<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_v3 extends CI_Controller {

	function __construct()
	{
        parent::__construct();

		$this->load->database();

        $this->load->library('Wtnh_google_cal_v2');
		//$this->load->library('Caspio_events_cal_v2');
        $this->load->library('Studio8feeds');

    }

	public function index()
	{

		// Load Database
		$this->load->model('statusboard_model');

		// Slide show data
		$data['slideshow'] = $this->grabNews8SlideShow();

		// Create data to pass
		$HomePageDate =  date('F jS Y',time());

		// Assign to data array
		$data['todaysHomePageDate'] = $HomePageDate;



		//$data['ann'] = $this->caspio_events_cal_v2->showAnnStartDates();
		$data['ann'] = $this->statusboard_model->getStudioAnn();

		//$data['bdays'] = $this->caspio_events_cal_v2->showBirthdayStartDates();
		$data['bdays'] = $this->statusboard_model->getStudioBDay();

		$data['stationevents'] = $this->wtnh_google_cal_v2->showStationCal(3);


		// Pass the array
		$this->load->view('board_v3',$data);
	}


	function grabNews8SlideShow(){

		$arr = array();

		$feed_url = "http://studio8.wtnh.com/wordpress/tag/statusboard-slideshow/feed/";

		$content = file_get_contents($feed_url);
		$x = new SimpleXMLElement($content);

		$photogallery = $x->channel;
		$gallerytitle = $photogallery->item->title;
		$slidescontent = $photogallery->item->children('content',true)->encoded;

		// Grab slide show title
		$arr[] = (string) $gallerytitle;

		$slides = explode("</a>", $slidescontent);

		array_pop($slides);

		//Grab the title and the URL to the page
		foreach($slides as $key){

			// Grab descriptipn
			preg_match('/alt="(.*?)"/',$key,$alt);

			// Grab img
			preg_match('/src="(?P<image>.*\.(jpeg|png|jpg))"/',$key,$out);

			// Chop off image size
			$i = str_replace("-150x150","",$out['image']);

			$arr[] = array( "image" => $i,"desc"=> $alt[1] );

		 }

		//print_r($arr);
		return $arr;

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/board_v3.php  */