<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_v0 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
        parent::__construct();
        
        $this->load->library('Studio8feeds');
        $this->load->library('Wtnh_google_cal');
    }	 
	 
	public function index()
	{
		
		// Create data to pass 
		$HomePageDate =  date('F jS Y',time());
		
		$data['wtnhsch'] = $this->studio8feeds->showSchedule('http://wtnh.titantv.com/apg/ttv.aspx?siteid=49965');
		$data['wtnhsch_title'] = "WTNH";
		$data['wctxsch'] = $this->studio8feeds->showSchedule('http://wctx.titantv.com/apg/ttv.aspx?siteid=51071');
		$data['wctxsch_title'] = "WCTX";
		
		$data['stationevents'] = $this->wtnh_google_cal->showStationCal(5);
	
		// Assign to data array
		$data['todaysHomePageDate'] = $HomePageDate;
		
		// Pass the array
		$this->load->view('board_v0',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */