<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_v2 extends CI_Controller {

	function __construct()
	{
        parent::__construct();
        
		$this->load->library('Studio8feeds');
        $this->load->library('Wtnh_google_cal');
        $this->load->library('Caspio_events_cal');
    
    }	 
	 
	public function index()
	{
		
		// Create data to pass 
		$HomePageDate =  date('F jS Y',time());
		
		// Grab Slide Show Data from WordPress Studio8 Blog
		$currentSlideShow = $this->grabSlideShow();
		
		if(count($currentSlideShow)>0){
			foreach($currentSlideShow as $k => $v){				
				$data['slideshow_title'] = $k;				
				$slidesurl = $v;
			};
		} else {
			$data['slideshow_title'] = "WTNH Studio Photos";
		}
	
		//http://studio8.wtnh.com/wordpress/2013/10/pbi-gallery-test/?json=1
		if(isset($slidesurl)){
			$photos = $this->studio8feeds->getSlides($slidesurl);
		
			foreach ($photos as $p){
				preg_match('/src="(?P<image>.*\.(jpeg|png|jpg))"/',$p, $out);
				$i = str_replace("-150x150","",$out['image']);
				$data['slideshow'][] = $i;	
			}	
		}
	
		$data['wtnhsch'] = $this->studio8feeds->showSchedule('http://wtnh.titantv.com/apg/ttv.aspx?siteid=49965');
		$data['wtnhsch_title'] = "WTNH";
						
		// Assign to data array
		$data['todaysHomePageDate'] = $HomePageDate;
		
		$data['ann'] = $this->caspio_events_cal->showEventsCal();
		$data['stationevents'] = $this->wtnh_google_cal->showStationCal(5);
		
		// Pass the array
		$this->load->view('board_v2',$data);
	}
	
	
	function grabSlideShow(){
		
		$arr = array();	
				
		$feed_url = "http://studio8.wtnh.com/wordpress/tag/statusboard-slideshow/feed/";
		
		$content = file_get_contents($feed_url);
		$x = new SimpleXMLElement($content);
		$channels = $x->channel;
		$pages = $channels->item;
		foreach($pages as $page){
			$arr["$page->title"] = $page->link;
		}
		
		return $arr;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/board_v2.php */