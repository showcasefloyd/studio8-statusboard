<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wtnh_google_cal_v2 {

	function parseCalendar($feed_url, $count = 6){

		$sid = 'gCal';
		$cid = 'WTNH';
		$crss = './cache/cached_'.$sid.'_'.$cid.'.xml';
		// 300 - 5 min
		// 600 = 10 min
		// 1200 = 20 min
		// 2400 = 40 mins
		$ctime = '1200';

		// Check for the our local non-cache_lite file
		if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
		{
			//echo "creating a new cache file $crss <br><br>";
			// No create a new one
			$raw = file_get_contents($feed_url);
			$content = str_replace('&#194','',$raw);
			//$content = utf8_encode($raw);

			$fp = fopen('./cache/cached_'.$sid.'_'.$cid.'.xml', 'w+');
			fwrite($fp, $content);
			fclose($fp);

		} else {

			//echo "reading from the GCAL cache <br><br>";
			// cache is good load it
			$content = file_get_contents($crss);
		}

		$x = new SimpleXMLElement($content);
		$entries = $x->entry;
		$arr = array();

		for ($i=0;$i < count($entries); $i++){
			$item = explode("<br />", $entries[$i]->content);
			array_unshift($item, (string) $entries[$i]->title);

			foreach($item as $k => $v){

				//if blank line or event status --dump it
				//2 == is blank, 4 is status
				if($k === 2 || $k === 4){
					unset($item[$k]);
				}
			}
			// reset array keys to correct numbers
			$item = array_values($item);
			//print_r($item);

			array_push($arr, $item);
		}
		// Reverse - show the most current first
		//$arr = array_reverse($arr);

		// Limit to the number passed in
		$arr = array_slice($arr,0,$count);
		return $arr;


	}

	function showStationCal($num = 8){

		$cal = array();

		//$feed = "https://www.google.com/calendar/feeds/dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com/private-c1690f28372e9ad0623d9219d9857a87/basic";

		$feed = "https://www.google.com/calendar/feeds/dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com/private-c1690f28372e9ad0623d9219d9857a87/basic?orderby=starttime&max-results=15&sortorder=ascending&futureevents=true";

		$events = $this->parseCalendar($feed, $num);

		foreach($events as $e){

			$i = '<div class="cal-event">';
				$i .= '<span class="title">'.$e[0] .'</span>' ;
				if(isset($e[1])){

					$patterns = array();
					$patterns[0] = '/When:/';
					$patterns[1] = '/EST/';
					$replacements = array();
					$replacements[1] = '';
					$replacements[0] = '';
					$i .= preg_replace($patterns, $replacements, $e[1]) ."<br>";

					//$i .= str_replace("When:","",$e[1]) ."<br>" ;
				}

				/* $i .= str_replace("Where:","",$e[2])  ."<br>"  */;

				if(isset($e[3])){
					$i .= str_replace("Event Description:","", substr($e[3],0,90)) ."<br>" ;
				}
			$i .= '</div>';

			$cal[] = $i;
		}

		return $cal;
	}

}
/* End of file Akamai-Upload.php */