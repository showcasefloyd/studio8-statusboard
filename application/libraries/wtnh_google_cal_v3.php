<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wtnh_google_cal_v3 {

	function parseCalendar($feed_url){
		
		$sid = 'gCal';
		$cid = 'WTNH';
		$crss = './cache/cached_'.$sid.'_'.$cid.'.xml';
		// 300 - 5 min
		// 600 = 10 min
		// 1200 = 20 min
		// 2400 = 40 mins
		$ctime = '1200';

		// Check for the our local non-cache_lite file
		if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
		{
			//echo "creating a new cache file $crss <br><br>";
			// No create a new one
			$raw = file_get_contents($feed_url);
			$content = str_replace('&#194','',$raw);
			//$content = utf8_encode($raw);

			$fp = fopen('./cache/cached_'.$sid.'_'.$cid.'.xml', 'w+');
			fwrite($fp, $content);
			fclose($fp);

		} else {
			
			//echo "reading from the GCAL cache <br><br>";
			// cache is good load it
			$content = file_get_contents($crss);
		}
		
		//var_dump(json_decode($content));
		$data = json_decode($content);
	
		return $data;

	}

	function showStationCal($maxResults = 6){

		$cal = array();
		$timeMin = date(DATE_RFC3339);

		/* 
		 Notes: This is using the Google Cal API 
		 
		 THIS IS THE EVENTS API
		 https://developers.google.com/google-apps/calendar/v3/reference/events/list
		 
		 I'm passing in the calendr ID: dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com
		 I'm calling the events method
		 I'm appending this params: 
		 	?maxResults=12&
		 	orderBy=startTime& 
		 	singleEvents=true&
		 	timeMin=2015-10-05T17%3A00%3A41-04%3A00 (must be a DATE_RFC3339 format)
		 	&key=AIzaSyALZZZLUbs5DT7BdwBryWGGaxebhwekjV4 
		 The key is my Server API Key: AIzaSyALZZZLUbs5DT7BdwBryWGGaxebhwekjV4
		 
		*/
		$feed = 'https://www.googleapis.com/calendar/v3/calendars/dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com/events?maxResults='.$maxResults.'&orderBy=startTime&singleEvents=true&timeMin='.$timeMin.'&key=AIzaSyD9U2IM30IX5QVN7p-M5Y88jrk1sRchPr0';
		
		$events = $this->parseCalendar($feed);
		
		
		if(!empty($events)){
		
			foreach($events->items as $e){
				
				$i = '<div class="cal-event">';
					$i .= '<span class="title">'.$e->summary .'</span>' ;
					if(isset($e->start->dateTime)){
						$i .= date("D M dS g:ia" ,strtotime($e->start->dateTime)) ." to ". date("g:ia" ,strtotime($e->end->dateTime));
					}
					if(isset($e->description)){
						$i .= "<br>".$e->description;
					}
				$i .= '</div>';
	
				$cal[] = $i;
			}
	
			return $cal;
		} else {
			
			$i = '<div class="cal-event"> <span class="title">Calendar currently unavailable</span></div>';
			$cal[] = $i;
			
			return $cal;
		}
	}

}
/* End of file Akamai-Upload.php */