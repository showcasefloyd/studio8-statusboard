<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include('simple_html_dom.php');  

class Studio8feeds {
	
    function getSlides($url){
	
		$html = file_get_html($url);
		
		$sch = array();
		//$addLength = "";
		
		# get an element representing the second paragraph  
		//$today = $html->find('.ghcNow',0)->plaintext;
		//$startGridTime = $html->find('.gHCTime',0)->plaintext;
		$WTNHPhotoGallery = $html->find('#gallery-1',0);
	
		//print "<h4>Coming up on ". $station." </h4>";
		//$getUnixTimeStamp = strtotime($startGridTime);
	
		//$nextStartTime = $startGridTime;
		foreach($WTNHPhotoGallery->find('img') as $Cell){
			
			$sch[] = $Cell;
			//if(isset($Cell->find('#tn',0)->plaintext)){
				
				//$length = $Cell->colspan;
				//$title = $Cell->find('#tn',0)->plaintext;
			
				//echo $nextStartTime ." - " . $title . " ( " . $length . " mins )<br>" ;
				//$sch[] = $nextStartTime ." - " . $title . " ( " . $length . " mins )<br>" ;
	
				//$addLength += $length;
	
				//$nextStartTime = date("h:i A", strtotime("+$addLength minutes", $getUnixTimeStamp ));
			//}
			
		}
		return $sch;
	}	
	
	
	function showSchedule($url,$station = "Station Name"){
	
		$html = file_get_html($url);
	
		$sch = array();
		$addLength = "";
		
		# get an element representing the second paragraph  
		$today = $html->find('.ghcNow',0)->plaintext;
		$startGridTime = $html->find('.gHCTime',0)->plaintext;
		$WTNHSchedule = $html->find('.gridTable',0);
	
		//print "<h4>Coming up on ". $station." </h4>";
		$getUnixTimeStamp = strtotime($startGridTime);
	
		$nextStartTime = $startGridTime;
		foreach($WTNHSchedule->children(1)->find('td') as $Cell){
			if(isset($Cell->find('#tn',0)->plaintext)){
				
				$length = $Cell->colspan;
				$title = $Cell->find('#tn',0)->plaintext;
			
				//echo $nextStartTime ." - " . $title . " ( " . $length . " mins )<br>" ;
				//$sch[] = $nextStartTime ." - " . $title . " ( " . $length . " mins )<br>" ;
				$sch[] = $nextStartTime ." - " . $title ."<br>" ;
				
				$addLength += $length;
	
				$nextStartTime = date("h:i A", strtotime("+$addLength minutes", $getUnixTimeStamp ));
			}
			
		}
		return $sch;
	}	
	
}
/* End of file Akamai-Upload.php */