<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Videofeed {

	var $ctime = '600';

	// Pulls Anniversaries from the Employee Database
	function readfeed(){


			$sid = 'forecast_video';
			$cid = 'WTNH';
			$crss = './cache/cached_'.$sid.'_'.$cid.'.txt';

			// Check for the our local non-cache_lite file
			if (!file_exists($crss) || (time() - filemtime($crss) >= $this->ctime) || (isset($poison) && $poison==TRUE) )
			{

				$url = file_get_contents('http://wtnh.com/category/weather/mobile-forecast/feed/');
				$url = utf8_encode($url);

				$fp = fopen($crss, 'w+');
				fwrite($fp, $url);
				fclose($fp);

				$file = file_get_contents($crss);

			}  else {

				echo "reading from the Video Feed cache <br><br>";

				// cache is good load it

				//Grab RSS
				$file = file_get_contents($crss);

			}

			//Convert to XML Object
			$x = new SimpleXMLElement($file);
			// Grab Just the HTML
			$html = $x->channel->item->children("content",true);


		    $doc = new DOMDocument();
			$doc->loadHTML($html);
			$imageTags = $doc->getElementsByTagName('iframe');

			$i = 0;
			foreach($imageTags as $tag) {

				if($i == 1){
					break;
				}
				$youTube = $tag->getAttribute('src');
				$i++;
			}
			echo "<iframe class='youtube-player' type='text/html' width='300' src='$youTube' frameborder='0'></iframe>";



		}

	}




/* End of file Caspio_events_cal.php */