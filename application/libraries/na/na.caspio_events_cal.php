<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  

class Caspio_events_cal {

	var $display_num = 7;
	
	//Caspio Bridge WS API WSDL file
	var $wsdl = "https://b2.caspio.com/ws/api.asmx?wsdl";

	//Caspio Bridge table with sample data
	var $tableName = "Calendar_of_Events_2013";

	//Caspio Bridge account name
	//$name = isset($_POST["_name"])? $_POST["_name"]:"LINTV";
	var	$name = "sdonnarummo";

	//Web service profile
	var	$profile = "wtnhwebservice";

	//Web service profile password
	var	$password = "Hny1dUyqzf1ra4w6";

	// Original
    function showEventsCal(){
		try
		{
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($this->wsdl);
			$resFields = $client->GetTableDesignRaw( $this->name, $this->profile, $this->password,$this->tableName);
			//Event like '%Birthday' and	
			$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, $this->tableName, false, 'event,date', "Date > GETDATE()", "Date", "", "");
			
			$a = 0;
			$data = array();
			foreach($events as $e){
				
				// Limit Number
				if($a == 7) break;
				// Spit array 
				$n = explode(",",$e);
				// strip single quotes
				$p = substr($n[0],1, -1);
				// String Time stamp
				$g = substr($n[1],0, -11);
				// Push to an array
				array_push($data,$p,$g);
				
				$a++;
			}
			
			return $data;
		
		}	
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
			print($str);
		}
	}
	
	// Pulls Anniversaries from the Employee Database
	function showAnnStartDates(){
		try
		{
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($this->wsdl);
			$resFields = $client->GetTableDesignRaw( $this->name, $this->profile, $this->password,"Employee_Database");
			//Event like '%Birthday' and	
			//$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, "Employee_Database", false,"", "MONTH(Hire_Date) >= MONTH(GETDATE()) AND DAY(Hire_Date) > DAY(GETDATE())", "MONTH(Hire_Date),DAY(Hire_Date)", "", "");
			
			$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, "Employee_Database", false,"", "datepart(dy, Hire_Date) >= datepart(dy, GETDATE())", "MONTH(Hire_Date),DAY(Hire_Date)", "", "");
			
			$a = 0;
			$data = array();
			foreach($events as $e){
				
			
				// Limit Number
				if($a == $this->display_num) break;
				
				// Spit array 
				$n = explode(",",$e);
				// Grab just the anniversity date and month
				$an = explode("/",$n[4]);
				//strip single quotes
				$p = substr($n[2],1, -1);
				// String Time stamp
				$g = substr($n[1],1, -1);
				// Push to an array
					
				// Show the number of years			
				$yos = ($n[5] == 'NULL'  ) ? ' ' : '('.$n[5] . ' yrs)';
				
				$s = "<span class='station-ann'>".$p .' '. $g ."</span> - " . $an[0] .'/'. $an[1] . ' '. $yos ;
				
				//print_r($n);
				
				array_push($data,$s);
				
				$a++;
			}

			return $data;
		
		
		}	
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
			print($str);
		}
	}
	
	// Need to the Bdays next (just copy above table)
	function showBirthdayStartDates(){
		try
		{
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($this->wsdl);
			$resFields = $client->GetTableDesignRaw( $this->name, $this->profile, $this->password,"Employee_Database");
			
			//Event like '%Birthday' and	
			//$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, "Employee_Database", false, "", "MONTH(Birth_Date) >= MONTH(GETDATE()) AND DAY(Birth_Date) > DAY(GETDATE())", "MONTH(Birth_Date),DAY(Birth_Date)", "", "");
			
			$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, "Employee_Database", false, "", "datepart(dy, Birth_Date) >= datepart(dy, GETDATE())", "MONTH(Birth_Date),DAY(Birth_Date)", "", "");
			
			$a = 0;
			$data = array();
			foreach($events as $e){
			
				//print($e) ."<BR><BR>";
				
				// Limit Number
				if($a == $this->display_num) break;
				
				// Spit array 
				$n = explode(",",$e);
				
				// Grab just the birthday date and month
				$br = explode("/",$n[3]);
				//strip single quotes
				$p = substr($n[2],1, -1);
				// String Time stamp
				$g = substr($n[1],1, -1);
				// Push to an array
					
				// Show the number of years			
				//$yos = ($n[5] == 'NULL'  ) ? ' ' : '('.$n[5] . ' yrs)';
				
				$s = "<span class='station-ann'>".$p .' '. $g ."</span> - ". $br[0] .'/'. $br[1];

				array_push($data,$s);
				
				$a++;
			}
			
			//print_r($data);

			return $data;
		}	
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
			print($str);
		}
	}
	
	function showStationBirthdays(){	
		try
		{
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($this->wsdl);
			$resFields = $client->GetTableDesignRaw($this->name, $this->profile, $this->password, $this->tableName);
			//Event like '%Birthday' and	
			$bdays = $client->SelectDataRaw($this->name, $this->profile, $this->password, $this->tableName, false, 'event,date', "Event like '%Birthday' and Date > GETDATE()", "Date", "", "");
			
			$a = 0;
			$data = array();
			foreach($bdays as $b){
				
				// Limit Number
				if($a == $this->display_num) break;
				// Spit into an array 
				$n = explode(",",$b);
			
				// Remove the word Birthday
				$n[0] = str_replace("'s Birthday"," - ", $n[0]);
				
				// strip single quotes
				$p = substr($n[0],1, -1);
	
				// Strip Time stamp
				$g = substr($n[1],0, -11);
				
				// Push to an array
				array_push($data,$p,$g);
				
				$a++;
			}
			
			return $data;
		
		}	
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
			print($str);
		}

	}

	function showStationAnnvDates(){	
		try
		{
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($this->wsdl);
			$resFields = $client->GetTableDesignRaw($this->name, $this->profile, $this->password, $this->tableName);
			//Event like '%Birthday' and	
			$events = $client->SelectDataRaw($this->name, $this->profile, $this->password, $this->tableName, false, 'event,date', "Event like '%Anniversary' and Date > GETDATE()", "Date", "", "");
			
			$a = 0;
			$data = array();
			foreach($events as $e){
				
				// Limit Number
				if($a == $this->display_num) break;
				// Spit array 
				$n = explode(",",$e);
				
				$n[0] = str_replace("'s Anniversary"," - ", $n[0]);
				
				// strip single quotes
				$p = substr($n[0],1, -1);
				// String Time stamp
				$g = substr($n[1],0, -11);
				// Push to an array
				array_push($data,$p,$g);
				
				$a++;
			}
			
			return $data;
		}	
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
			print($str);
		}

	}

}
		
/* End of file Caspio_events_cal.php */