<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WTNH Studio Status Board</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/status.css">
</head>
<body>

  	<div class="container">
  		<div class="row">
  			<div id="wtnh-welcome-widget" class="col-lg-8">
				<h2><?php echo $todaysHomePageDate; ?></h2>
			</div>
	  		<div id="wtnh-sch-widget" class="col-lg-4">
	  			
	  			<h3>Birthdays / Anniversary Calendar</h3>
	        	
	        	<?php
	        	for ($i = 0; $i < count($ann); $i++)
				{
				  if ($i % 2 == 0)
				  {
				    print "<span class='station-ann'>".$ann[$i]."</span> ";
				  }
				  else
				  {
				    print "<span>".$ann[$i]."</span><br>";

				  }
				}
					        	
	        	
	        	?>
	  		</div>
  		</div>	<!-- /First Row -->
  		
  		<div class="row">
	  		<div class="col-lg-8">
	  			<h3><?php echo $slideshow_title; ?></h3>
	  			
	  			<div id="carousel-wtnh-photos" class="carousel slide">

					<ol class="carousel-indicators">
					  <!--
					  <li data-target="#carousel-wtnh-photos" data-slide-to="0" class="active"></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="1" class=""></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="2" class=""></li>
					  -->
					  <?php
					  //for($f=0;$f<count($slideshow);$f++){
						  //echo '<li data-target="#carousel-wtnh-photos" data-slide-to="'.$f.'" class="'. ($f == 0 ? 'active' : '')  .'"></li>';
					  //}
					  ?>
					</ol>
					
       			    <div class="carousel-inner">
  
          			  <?php
          			  if(isset($slideshow)){ 
	          			  $s = 0;
	          			  foreach($slideshow as $slide){
	          			  	echo '<div class="item '. ($s == 0 ? 'active' : '') .' ">';
				            echo '<img width="720" height="405" src="'. $slide .'" alt="'. $s .' image">';
							echo '</div>';
							$s++;
	          			  }
          			  } else {
	          			  
	          			  echo "<h3> No slides found </h3>";
          			  }
          			  ?>
			             
			        </div>
					
			    </div>
	  		</div>
	  		<div id="wtnh-birthday-calendar" class="col-lg-4">
	        	<h3>Station Events Calendar</h3>
	        	<?php foreach ($stationevents as $event){
	        		print $event;
	        	}
	        	?>
	  		</div>
	  	</div> <!-- /Second Row -->
  		
	</div><!-- /Container -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
	
	<script>
	$(document).ready(function() {   
	
		// TWICKER
		var ticker=document.createElement('script');ticker.type='text/javascript';ticker.async=false;ticker.src='//twitcker.com/ticker/WTNH.js?speed=2&count=12&background=ffffff&tweet=ffffff&links=true&container=footer';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(ticker);	

	   	 		
	   	 // WTNH Photos
	   	$('#carousel-wtnh-photos').carousel({interval: 6000});
	   	$('#carousel-wtnh-photos').carousel('cycle');
	   	 		
		// Pull in Message of the day
		$.ajax({
			dataType: "jsonp",
			url: 'http://studio8.wtnh.com/wordpress/2013/10/status-board/?json=1',
			success: function(data){
				 //console.log(data.page['content']);
				 motd = data.post['content'];
				 if(motd != ""){
				 	$('#wtnh-welcome-widget').append('<span class="">'+motd+'</span>');  
				 } else {
					 $('#wtnh-welcome-widget').append('<span class="">Welcome to WTNH / WCTX</span>'); 
					 
				 }
			}
		});

	});	
    </script>

</body>
</html>