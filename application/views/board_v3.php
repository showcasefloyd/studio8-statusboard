<html lang="en">
<head>
	<meta charset="utf-8">
	<!--  <meta http-equiv="refresh" content="1800"> ever 30 mins -->
	<title>WTNH Studio Status Board</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/simpletextrotator.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/status_1080.css">
</head>
<body>

  	<div class="container">
  		<div class="row">
  			<div id="wthn-date" class="col-lg-12"><h1><?php echo $todaysHomePageDate; ?></h1></div>
  		</div>	<!-- /First Row -->

  		<div class="row">
  			<div id="wtnh-welcome-message" class="col-lg-12"></div>
  		</div>	<!-- /First Row -->

  		<div class="row">
  			<div id="wtnh-google-calendar" class="col-lg-3">
	        	<h2>Station Events Calendar</h2>
	        	<?php foreach ($stationevents as $event){
	        		print $event;
	        	}
	        	?>
	  		</div>

	  		<div class="col-lg-6">
	  			<?php if($slideshow[0] != ""){
	  			 	echo '<h2>'. $slideshow[0] .'</h2>';
	  			} ?>

	  			<div id="carousel-wtnh-photos" class="carousel slide">

       			    <div class="carousel-inner">

          			  <?php
          			  if(isset($slideshow)){

	          			 for($s = 1; $s < count($slideshow); $s++){
	          			  	echo '<div class="item '. ($s == 1 ? 'active' : '') .' ">';
				            echo '<img src="'. $slideshow[$s]['image'] .'" alt="'. $s .' image">';
				            echo ' <div class="carousel-caption"> ';
							echo ' <h3> '.$slideshow[$s]['desc'].' </h3>';
							echo ' </div>';
							echo '</div>';
	          			 }
          			  } else {
	          			  echo "<h3> No slides found </h3>";
          			  }
          			  ?>

			        </div>

			    </div>
	  		</div>

	  		<div id="wtnh-birthday-calendar" class="col-lg-3">

	  			<div style="margin-bottom:22px;">
		  			<h2>Anniversaries</h2>
		        	<?php
			        $todayMonthDay = date('n/j',time());

			        foreach ($ann as $row)
					{
						$adate = date_parse($row->Hire_Date);
						$monthDay = $adate['month']."/".$adate['day'];

						//$n = ($row->numberofyears) ? "(". $row->numberofyears ." Yrs)" : '(1 Yr)';
						if($row->numberofyears == "0"){
							$n = "(1 Yr)";
						} else {
							if( $todayMonthDay == $monthDay){
								$n = "(". ($row->numberofyears) ." Yrs)";
							} else {
								$n = "(". ($row->numberofyears + 1) ." Yrs)";
							}
						}

						echo "<span class='station-ann'>".$row->First_Name ." ". $row->Last_Name ."</span> - ". $monthDay . " ". $n ." <br>";
						//echo ($row->numberofyears != "0") ? $row->numberofyears : "pink" ."<br><br>";
					}

					?>
	  			</div>
	  			<div style="margin-bottom:22px;">
					<h2>Birthdays</h2>
			        <?php
					foreach ($bdays as $row)
					{
						$bdate = date_parse($row->Birth_Date);
						echo "<span class='station-ann'>".$row->First_Name ." ". $row->Last_Name ."</span> - ".$bdate['month']."/".$bdate['day']."<br>";
					}

					?>
	  			</div>
	  		</div>


	  	</div> <!-- /Second Row -->

	</div><!-- /Container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery.simple-text-rotator.js"js/></script>

	<script>
	$(document).ready(function() {


		// Pull in Message of the day
		$.ajax({
			dataType: "jsonp",
			url: 'http://studio8.wtnh.com/wordpress/2013/10/status-board/?json=1',
			success: function(data){

				 //console.log(data.page['content']);
				 motd = data.post['content'];
				 // remove blank kiube returns
				 mot = motd.replace(/(\r\n|\n|\r)/gm,'');
				 //console.log(mot);

				 // count the # of ** in the string
				 c = (motd.split("**").length - 1);

				 if(c == 0){
				 	$('#wtnh-welcome-message').append('<span class="">Welcome to WTNH / WCTX</span>');
				 }  else  {

				 	// Checks if someone left the ** in it
				 	//if (c == 1){
					// 	motd = motd.replace('**',' ');
				 	//}

					$('#wtnh-welcome-message').append('<span class="rotate">'+mot+'</span>');
				 }

				 // Only animate if there is more then one item
				 if(c > 1) {
					 $(".rotate").textrotator({
						 animation: "dissolve", //Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
						 separator: "**",
						 speed: 9000 // How many milliseconds until the next word show.
					 });
				 }
			}
		});


	   	 // WTNH Photos
	   	$('#carousel-wtnh-photos').carousel({interval: 10000});
	   	$('#carousel-wtnh-photos').carousel('cycle');

		// TWICKER
		var ticker=document.createElement('script');ticker.type='text/javascript';ticker.async=false;ticker.src='//twitcker.com/ticker/WTNH.js?speed=2&count=12&background=ffffff&tweet=ffffff&links=true&container=footer';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(ticker);
		//console.log($('#twitcker-marquee .box a').html());
		//$('#twitcker-marquee .box a').html().replace(/RT /g,"<span style='color:red;'>RT </span>");

	});
    </script>

</body>
</html>