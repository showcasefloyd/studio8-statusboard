<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="refresh" content="1800"> <!-- ever 30 mins -->
	<title>WTNH Studio Status Board</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/simpletextrotator.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/status_720.css">
</head>
<body>

  	<div class="container">
  		<div class="row">
  			<div id="wthn-date" class="col-lg-12"><h2><?php echo $todaysHomePageDate; ?></h2></div>
  		</div>	<!-- /First Row -->

  		<div class="row">
  			<div id="wtnh-welcome-message" class="col-lg-12"></div>
  		</div>	<!-- /First Row -->
  		
  		
  		<div class="row">
  			<div id="wtnh-google-calendar" class="col-lg-3">
	        	<h4>Station Events Calendar</h4>
	        	<?php foreach ($stationevents as $event){
	        		print $event;
	        	}
	        	?>
	  		</div>
  			
	  		<div class="col-lg-6">
	  			<h4><?php echo $slideshow_title; ?></h3>
	  			
	  			<div id="carousel-wtnh-photos" class="carousel slide">

					<ol class="carousel-indicators">
					  <!--
					  <li data-target="#carousel-wtnh-photos" data-slide-to="0" class="active"></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="1" class=""></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="2" class=""></li>
					  -->
					  <?php
					  //for($f=0;$f<count($slideshow);$f++){
						  //echo '<li data-target="#carousel-wtnh-photos" data-slide-to="'.$f.'" class="'. ($f == 0 ? 'active' : '')  .'"></li>';
					  //}
					  ?>
					</ol>
					
       			    <div class="carousel-inner">
  
          			  <?php
          			  if(isset($slideshow)){ 
	          			  $s = 0;
	          			  foreach($slideshow as $slide){
	          			  	echo '<div class="item '. ($s == 0 ? 'active' : '') .' ">';
				            echo '<img width="720" height="405" src="'. $slide .'" alt="'. $s .' image">';
							echo '</div>';
							$s++;
	          			  }
          			  } else {
	          			  
	          			  echo "<h3> No slides found </h3>";
          			  }
          			  ?>
			             
			        </div>
					
			    </div>
	  		</div>
	  		
	  		<div id="wtnh-birthday-calendar" class="col-lg-3">
	  			
	  			<h4>Birthdays / Anniversary Calendar</h4>
	        	
	        	<?php
	        	for ($i = 0; $i < count($ann); $i++)
				{
				  if ($i % 2 == 0)
				  {
				    print "<span class='station-ann'>".$ann[$i]."</span> ";
				  }
				  else
				  {
				    print "<span>".$ann[$i]."</span><br>";

				  }
				}
				?>
				
				
	  			<h4>Schedule</h4>
	    		<?php
	    			print "<span class='id'>Coming up on $wtnhsch_title</span>";
	    			foreach($wtnhsch as $p1){
		    			print $p1;
	    			}
	    		?>	
	  		</div>	        	
	        	
	      
	  	</div> <!-- /Second Row -->
  		
	</div><!-- /Container -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery.simple-text-rotator.min.js"js/></script>
	
	<script>
	$(document).ready(function() {   
	
	   		   	 		
		// Pull in Message of the day
		$.ajax({
			dataType: "jsonp",
			url: 'http://studio8.wtnh.com/wordpress/2013/10/status-board/?json=1',
			success: function(data){
				 
				 //console.log(data.page['content']);
				 motd = data.post['content'];
				 
				 // count the # of ** in the string
				 c = (motd.split("**").length - 1);

				 if(c == 0){
				 	$('#wtnh-welcome-message').append('<span class="">Welcome to WTNH / WCTX</span>');
				 }  else  {
				 	
				 	// Checks if someone left the ** in it
				 	if (c == 1){
					 	motd = motd.replace('**',' ');
				 	}
				 	
					$('#wtnh-welcome-message').append('<span class="rotate">'+motd+'</span>'); 
				 }
				 
				 // Only animate if there is more then one item
				 if(c > 1) {
					 $(".rotate").textrotator({
						 animation: "dissolve", //Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
						 separator: "**",
						 speed: 3000 // How many milliseconds until the next word show.
					 });
				 }
			}
		});
		
		 		
	   	 // WTNH Photos
	   	$('#carousel-wtnh-photos').carousel({interval: 6000});
	   	$('#carousel-wtnh-photos').carousel('cycle');

		
		// TWICKER
		var ticker=document.createElement('script');ticker.type='text/javascript';ticker.async=false;ticker.src='//twitcker.com/ticker/WTNH.js?speed=2&count=12&background=ffffff&tweet=ffffff&links=true&container=footer';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(ticker);	

	});	
    </script>

</body>
</html>