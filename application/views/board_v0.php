<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WTNH Studio Status Board</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/status.css">
</head>
<body>

  	<div class="container">
  		<div class="row">
	        
	        <div id="wtnh-weather-widget" class="col-lg-3">
	       		<h3>Weather</h3>
	       		<h4><span id="clock"></span></h4>
		   		<div id="weather-widget"></div>
	  		</div>
  			<div id="wtnh-welcome-widget" class="col-lg-5">
				<h3>Welcome! </h3>
				<h4>Today is <?php echo $todaysHomePageDate; ?></h4>
			</div>
	  		<div id="wtnh-sch-widget" class="col-lg-4">
	  			<h3>Schedule</h3>
	    		<?php
	    			print "<h4>Coming up on $wtnhsch_title</h4>";
	    			foreach($wtnhsch as $p1){
		    			print $p1;
	    			}
	    			print "<h4>Coming up on $wctxsch_title</h4>";
	    			foreach($wctxsch as $p2){
		    			print $p2;
	    			}
	    		?>	
	  		</div>
  		</div>	<!-- /First Row -->
  		
  		<div class="row">
	        <div id="wtnh-google-calendar" class="col-lg-3">
	        	<h3>Station Events Calendar</h3>
	        	
	        	<?php foreach ($stationevents as $event){
	        		print $event;
	        	}
	        	?>
	  		</div>
	  		<div class="col-lg-5">
	  			<h3>Photos</h3>
	  		
	  			   <div id="carousel-wtnh-photos" class="carousel slide">
					
					<ol class="carousel-indicators">
					  <li data-target="#carousel-wtnh-photos" data-slide-to="0" class="active"></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="1" class=""></li>
					  <li data-target="#carousel-wtnh-photos" data-slide-to="2" class=""></li>
					</ol>
					
       			    <div class="carousel-inner">
          			  <div class="item active">
			            <img src="http://placehold.it/640x480/5DB9F2/fff" alt="First slide">
			            	<!-- <div class="carousel-caption">Get me out of the sun</div> -->
			          </div>
			          
			          <div class="item">
			            <img src="http://placehold.it/640x480/803080/fff" alt="Second slide">
			            	<!-- <div class="carousel-caption"> Looking good</div> -->
			          </div>
			          
			          <div class="item">
			            <img src="http://placehold.it/640x480/E48285/fff" alt="Third slide">
							<!-- <div class="carousel-caption">Fun Times</div> -->
			          </div>
			          
			        </div>
			      
					<!--
					<a class="left carousel-control" href="#carousel-wtnh-photos" data-slide="prev">
			          <span class="icon-prev"></span>
			        </a>
			        <a class="right carousel-control" href="#carousel-wtnh-photos" data-slide="next">
			          <span class="icon-next"></span>
			        </a>
					-->
			      </div>

	  		</div>
	  		<div id="top-stories-widget" class="col-lg-4">
	  			<h3>Top Stories on WTNH.com</h3>
	  			<div id="liveUsers"></div>
		    	<ul id="topStories"></ul>
		    	<!-- <canvas id="topChart" width="200" height="200"></canvas> -->
	  		</div>
	  	</div> <!-- /Second Row -->
  		
	</div><!-- /Container -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/simpleclock.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery.zweatherfeed.min.js" type="text/javascript"></script> 
    <script src="<?php echo base_url(); ?>js/chart.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/chartbeat.js" type="text/javascript"></script>
	
	<script>
	$(document).ready(function() {   
	   	 		
	   	 // WTNH Photos
	   	 $('#carousel-wtnh-photos').carousel({interval: 6000});
	   	 $('#carousel-wtnh-photos').carousel('cycle');
	   	 
	   	 // Weather Widget
	   	 $('#wtnh-weather-widget').weatherfeed(
	   	 	['2458410'],{
   	 		woeid: true,
	 		unit: 'f',
			image: true,
			country: true,
			highlow: true,
			wind: true,
			humidity: false,
			visibility: false,
			sunrise: true,
			sunset: true,
			forecast: false,
			link: false});
						
		// Pull in Message of the day
		$.ajax({
			dataType: "jsonp",
			url: 'http://studio8.wtnh.com/wordpress/2013/10/status-board/?json=1',
			success: function(data){
				 //console.log(data.page['content']);
				 motd = data.post['content'];
				 $('#wtnh-welcome-widget').append('<span class="">'+motd+'</span>');  
			}
		});
			
		// Chartbeat Data
		$.jChartbeat({apikey: 'e627b28af298f5b4e25447e175bf69ae', host: 'wtnh.com'});

		function getPeopleOnTheSite(){	
			$.jChartbeat.quickstats(
				 function(results) {
					 //console.log(rr.people);
					 $('#liveUsers').html('<h3><span class="label label-primary">People on the site: '+results.people+'</span></h3>');
				}
			);	
		}
		
		function getTopStories(){
		    var data = [];
		    var color=["#556270","#67a193","#9dc386","#FF6B6B","#C44D58","#556270","#67a193","#9dc386","#FF6B6B","#C44D58"];
		
		    $.jChartbeat.toppages(function(response) {
		    	/* console.log(response); */
		        $('#topStories').html('');
		        $.each(response, function(x) {
		        	
		        	newText = this.i.replace('WTNH -','');
		        	$('#topStories').append('<li style="color:'+color[x]+'"><div style="width:50px; float:left;">[ '+this.visitors+' ]</div> '+newText+'</li>');
		        	//Doughnut Chart
					//data.push({value: this.visitors, color: color[x]});
		        });
		
		    /*
			var ctx = document.getElementById("topChart").getContext("2d");
		    var myNewChart = new Chart(ctx).Doughnut(data,{
		    	percentageInnerCutout : 70, 
		    	segmentShowStroke: true, 
		    	animation: false,
		    	animateRotate : true});
			*/
		    });
		}
			
		// TWICKER
		//var ticker=document.createElement('script');ticker.type='text/javascript';ticker.async=true;ticker.src='//twitcker.com/ticker/WTNH.js?speed=1&count=11&background=ffffff&tweet=ffffff&links=true&hide-logo=false&container=footer';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(ticker);
		
		var ticker=document.createElement('script');ticker.type='text/javascript';ticker.async=true;ticker.src='//twitcker.com/ticker/WTNH.js?speed=3&count=13&background=ffffff&tweet=ffffff&container=footer';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(ticker); 
			
		getTopStories();
		getPeopleOnTheSite();
		updateClock();
		setInterval(function(){ getTopStories(); }, 3000);
		setInterval(function(){ getPeopleOnTheSite(); }, 1000 );
		setInterval(function(){ updateClock(); }, 1000 );	
	});	
    </script>

</body>
</html>