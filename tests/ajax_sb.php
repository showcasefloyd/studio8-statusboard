<?php  
ini_set('default_socket_timeout', 20);
error_reporting();

require_once('Cache/Lite.php');

//$Google_feed = "https://www.google.com/calendar/feeds/dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com/private-c1690f28372e9ad0623d9219d9857a87/basic?orderby=starttime&max-results=15&sortorder=ascending&futureevents=true";

//$sid = 'gCal';
//$cid = 'WTNH';
//$cache_id_1 = 'wtnh_events_calendar_feed';

$base_url = 'http://wtnhhosting.dev/wtnh-statusboard/';


// 300 = 5 mins

$cache_opts = array('cacheDir' =>  './cache/',
                'lifeTime' => 300, 
                'automaticCleaningFactor' => 80,
                'pearErrorMode' => CACHE_LITE_ERROR_DIE,
               );

$Cache = new Cache_Lite($cache_opts);

	
// Check for Cached File
if ($data = $Cache->get('wtnh_events_calendar_feed'))
{

	echo "FILES IS ALREADY CACHED FILE";
	
}  else {
	
	echo "<b>FILES HAS NOT BEEN CACHED <br><br></b>";
	
	$url = file_get_contents('https://www.google.com/calendar/feeds/dop6m4niscllf3a3rhoed1heag%40group.calendar.google.com/private-c1690f28372e9ad0623d9219d9857a87/basic?orderby=starttime&max-results=15&sortorder=ascending&futureevents=true');
	$url = utf8_encode($url);
	
	echo $url;
	
	$Cache->save($url);
	
}
?> 