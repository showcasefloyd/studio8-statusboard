<?php  
ini_set('default_socket_timeout', 20);
error_reporting();

$display_num = 7;
	
//Caspio Bridge WS API WSDL file
$wsdl = "https://b2.caspio.com/ws/api.asmx?wsdl";

//Caspio Bridge table with sample data
$tableName = "Calendar_of_Events_2013";

//Caspio Bridge account name
//$name = isset($_POST["_name"])? $_POST["_name"]:"LINTV";
$name = "sdonnarummo";

//Web service profile
$profile = "wtnhwebservice";

//Web service profile password
$password = "Hny1dUyqzf1ra4w6";

$display_num = 6;

// Pulls Anniversaries from the Employee Database
function showAnnStartDates(){
	
	global $wsdl,$tableName,$name,$profile,$password,$display_num ;
	
	$sid = 'birthdaydates';
	$cid = 'WTNH';
	$crss = './cache/cached_caspio_'.$sid.'_'.$cid.'.txt';
	$ctime = '300';

	try
	{

		// Check for the our local non-cache_lite file
		if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
		{
			echo "creating a new cache file $crss <br><br>";
		
			//init SOAP Client and get table fields description into $resFields array
			$client = new SoapClient($wsdl);
			$resFields = $client->GetTableDesignRaw( $name, $profile, $password,"Employee_Database");		
			$data = $client->SelectDataRaw($name, $profile, $password, "Employee_Database", false,"", "datepart(dy, Hire_Date) >= datepart(dy, GETDATE())", "MONTH(Hire_Date),DAY(Hire_Date)", "", "");
			
			$fp = fopen('./cache/cached_caspio_'.$sid.'_'.$cid.'.txt', 'w+');
			foreach($data as $d){
				fwrite($fp,$d ."\r\n");
			}
			fclose($fp);
			
			$file = file_get_contents($crss);
			$rows = explode("\r\n", $file);
			
			//print_r($rows);
			
			
		} else {
		
			echo "reading from the cache <br><br>";
		
			// cache is good load it
			$file = file_get_contents($crss);
			$rows = explode("\r\n", $file);
			
			//print_r($rows);
		}

			
		$a = 0;
		$data = array();
		foreach($rows as $e){
			
			// Limit Number
			if($a == $display_num) break;
			
			// Spit array 
			$n = explode(",",$e);
			// Grab just the anniversity date and month
			$an = explode("/",$n[4]);
			//strip single quotes
			$p = substr($n[2],1, -1);
			// String Time stamp
			$g = substr($n[1],1, -1);
			// Push to an array
				
			// Show the number of years			
			$yos = ($n[5] == 'NULL'  ) ? ' ' : '('.$n[5] . ' yrs)';
			
			$s = "<span class='station-ann'>".$p .' '. $g ."</span> - " . $an[0] .'/'. $an[1] . ' '. $yos ;
			
			print_r($n);
			
			array_push($data,$s);
			
			$a++;
		}

		return $data;
	
	
	}	
	catch (SoapFault $fault)
	{
		//SOAP fault handling
		
		$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
		print($str);
	}

}

showAnnStartDates();

?> 