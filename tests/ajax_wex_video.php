<?php  
ini_set('default_socket_timeout', 20);
error_reporting();

require_once('Cache/Lite.php');
require_once('SimplePie/simplepie.inc');
require_once('Smarty/libs/Smarty.class.php');
require_once('../includes/config.php');

$Smarty = new Smarty();
$Smarty->compile_check = 1;
$Smarty->debugging     = 0;
$Smarty->template_dir  = './templates/';
$Smarty->compile_dir   = './templates_c/';

$ad = (isset($_GET['ad']) && !empty($_GET['ad']) ? $_GET['ad'] : 0);
$pid = (isset($_GET['pid']) && !empty($_GET['pid']) ? $_GET['pid'] : 0);
$ver = (isset($_GET['ver']) && !empty($_GET['ver']) ? $_GET['ver'] : 'hi');
$sid = (isset($_GET['siteId']) && !empty($_GET['siteId']) ? $_GET['siteId'] : '20000');
$cid = (isset($_GET['categoryId']) && !empty($_GET['categoryId']) ? $_GET['categoryId'] : '10001');
$zne = (isset($_GET['zone']) && !empty($_GET['zone']) ? '/'.$_GET['zone'] : '');
if (isset($_GET['siteId']) && !empty($_GET['siteId']) && array_key_exists(strtolower($_GET['siteId']), $sites)) {
  $meta = $sites[$sid];
}

// if (isset($_GET['categoryId']) && !empty($_GET['categoryId']) && isset($_GET['siteId']) && !empty($_GET['siteId'])) {
#echo('http://www.woodtv.com/feeds/rssFeed?siteId='.$sid.'&obfType=RSS_VIDEO&categoryId='.$cid);
// }

$cache_id = 'video_'.$_GET['siteId'].'_'.$_GET['categoryId'].'_'.$pid;

$cache_opts = array('cacheDir' => './cache/',
                'lifeTime' => 0,
                'automaticCleaningFactor' => 80,
                'pearErrorMode' => CACHE_LITE_ERROR_DIE,
               );

$Cache = new Cache_Lite($cache_opts);

if ($ad % 3 == 0 || $ad == 0) {
  // GET CONTENTS OF RSS FEEDS
  $url = array();
  $result = array();
  $url = file('http://ad.doubleclick.net/adx/lin.'.$meta['site'].$zne.';dcmt=text/xml;pos=;tile=1;mod=wex_video;sz=5x1000;ord='.rand(0, 1000000000000000000).'?rand='.rand(0, 1000000000000000000));
  //$url = file('http://ad.doubleclick.net/adx/lin.wlin;dcmt=text/xml;pos=;tile=1;mod=wex_video;sz=5x1000;ord='.rand(0, 1000000000000000000).'?rand='.rand(0, 1000000000000000000));
 	
  foreach ($url as $line) {
    if (preg_match("|flvhigh|is", $line)) {
      $result['flv'] = preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line);
      if (preg_match("|CDATA|is", $result['flv'])) {
        $result['flv'] = preg_replace("|.*?http(.*)?\.flv.*|is", "http$1.flv", $result['flv']);
      }
    }
    if (preg_match("|ClickUrl|is", $line)) {
      $result['link'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
    if (preg_match("|CompanionImg|is", $line)) {
      $result['cimage'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
    if (preg_match("|CompanionImgUrl|is", $line)) {
      $result['clink'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
    if (preg_match("|SponsorImg|is", $line)) {
      $result['simage'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
    if (preg_match("|SponsorImgUrl|is", $line)) {
      $result['slink'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
    if (preg_match("|<Url>|is", $line)) {
      $result['dot'] = trim(preg_replace("|.*?CDATA\[(.*)?\]\].*|is", "$1", $line));
    }
  }

  if (count($result) > 0) {
    $Smarty->assign('meta', $meta);
    $Smarty->assign('data', $result);
    $html = $Smarty->fetch('300x100.tmpl');
    $spon = $Smarty->fetch('88x31.tmpl');

    #echo $result['flv'].'|||'.preg_replace("|\n|is", " ", $html).'||| |||'.$result['link'].'|||'.$pid.'|||'.($ad+1).'|||ad|||'.$spon.'|||';
    # 2010-09-01 13:35 
    # determine whether ad delivery needs to be reset to a default value upon delivery (or failure to deliver) or if incrementation is necessary
    
    echo $result['flv'].'|||'.preg_replace("|\n|is", " ", $html).'||| |||'.$result['link'].'|||'.$pid.'|||1|||ad|||'.$spon.'|||'; //had trl
  } else {
    echo '||| |||||| |||'.$pid.'|||1|||ad||| |||';
  }

} else {

	if ($data = $Cache->get($cache_id))
	{
		
		$parts = explode('|||', $data);
		if($parts[0]!='')
		{
			echo $data;
			$poison = FALSE;			
		}
		else
		{
			$data = FALSE;
			$poison = TRUE;
		}
	
	} 

  // we couldn't get the feed from cache_lite or the cache was poisoned
  if(!isset($data) || $data==FALSE)
  {
	
    $crss = './cache/rss_'.$sid.'_'.$cid.'.xml';
    $ctime = '300';

	// can we get our local non-cache_lite file?
	if (!file_exists($crss) || (time() - filemtime($crss) >= $ctime) || (isset($poison) && $poison==TRUE) )
	{
		$url = file_get_contents('http://www.woodtv.com/feeds/rssFeed?siteId='.$sid.'&obfType=RSS_VIDEO&categoryId='.$cid);
		$url = utf8_encode($url);

		$fp = fopen('./cache/rss_'.$sid.'_'.$cid.'.xml', 'w+');
		fwrite($fp, $url);
		fclose($fp);
	}

	// cache is good load it
	else
	{
		$url = file_get_contents($crss);
	}

	// make sure our feed (loaded into $url var from cache or live site) has content. if not, default to news
	$dom = new DOMDocument();
	if($dom->loadXML($url))
	{
		if(!$dom->getElementsByTagName("item")->item(0))
		{
			$url = file_get_contents('http://www.woodtv.com/feeds/rssFeed?siteId='.$sid.'&obfType=RSS_VIDEO&categoryId=10001');
			$url = utf8_encode($url);	
			
			$fp = fopen('./cache/rss_'.$sid.'_10001.xml', 'w+');
			fwrite($fp, $url);
			fclose($fp);
			
			error_log('EMPTY FEED: sid='.$sid.', cid='.$cid);	
		}
			
	}
		
    /* Original Code (2010-10-01)
    $url = file_get_contents('http://www.woodtv.com/feeds/rssFeed?siteId='.$sid.'&obfType=RSS_VIDEO&categoryId='.$cid);
    $url = utf8_encode($url);
    */

    $num = 0;
    $csv = array();
    
    #############################################################################
    
    $feed = new simplepie();
    $feed->set_raw_data($url);
    $feed->enable_cache(true);
    $feed->enable_order_by_date(false);
    $feed->set_timeout(20);
    $feed->init();
    
    if (!$feed->error())
	{
		$items = $feed->get_items();

		foreach ($items as $i => $item)
		{
		
		
		
			$data[$num]['ttl']  = $item->get_title();
			$data[$num]['lnk']  = $item->get_link();
			$data[$num]['dsc']  = trim(strip_tags(html_entity_decode(iconv("UTF-8", "CP1252", strip_accents($item->get_description()))),'<b>'));

			# $result[$key] = trim(preg_replace("|\s+|is", " " , strip_tags(iconv("UTF-8", "CP1252", (string)$value))));  

			# GET THUMBNAIL
			$img = $item->get_item_tags('http://search.yahoo.com/mrss/', 'content');
			
			$data[$num]['tmb'] = $img[0]['attribs']['']['url'];
			$data[$num]['med'] = str_replace('82_61', '140_105', $data[$num]['tmb']);
			$data[$num]['lrg'] = str_replace('82_61', '320_240', $data[$num]['tmb']);


			$vid = $item->get_item_tags("http://www.canvastek.com/rss/2.0/ctek/", "video");
			$data[$num]['vhi'] = $vid[0]['child']['http://www.canvastek.com/rss/2.0/ctek/']['vidflvhil'][0]['data'];
			$data[$num]['vlo'] = $vid[0]['child']['http://www.canvastek.com/rss/2.0/ctek/']['vidflvlo'][0]['data'];
			
			if (empty($data[$num]['vhi']))
			{
				unset($data[$num]); 
			}
			else
			{
				$num++;
			}
		
		}
    }
	
    if (!array_key_exists($pid, $data))
	{
      $pid = 0;
    }

    $Smarty->assign('meta', $meta);
    $Smarty->assign('data', $data[$pid]);
   
    $html = $Smarty->fetch('main.tmpl');
    $html = $data[$pid]['vlo'].'|||'.preg_replace("|\n|is", " ", $html).'||| |||'.$data[$pid]['lnk'].'|||'.($pid+1).'|||'.($ad+1) .'|||'. $data[$pid]['lrg'];
  
    echo $html;
    $Cache->save($html);
  
  }
}

function strip_accents($text) {
  $text = preg_replace("|&#160;|is", " ", $text);
  $arr = array(
    chr(133) => '...', # Ellipsis 2
    chr(145) => '\'', # Left Single Quote 3
    chr(146) => '\'', # Right Single Quote 3
    chr(147) => '"', # Left Double Quote 3
    chr(148) => '"', # Right Double Quote 3
    chr(150) => '-', # M-Dash 5
    chr(151) => '-', # M-Dash 6
    chr(160) => ' ', # Non-breaking Space
    chr(169) => '(C)', # Copyright 2
    chr(188) => '1/2', # Vulgar One Half
    chr(189) => '1/4', # Vulgar One Quarter
    chr(194).chr(169) => '(C)', # Copyright
    chr(194).chr(171) => '<<', # Left Double Arrow
    chr(194).chr(180) => '\'', # Accent
    chr(194).chr(187) => '>>', # Right Double Arrow
    chr(195).chr(159) => 'B', # Small sharp s 1
    chr(195).chr(161) => 'a', # Latin a with accent
    chr(195).chr(164) => 'a', # Latin a diaeresis
    chr(226).chr(128).chr(147) => '-', # M-Dash 1
    chr(226).chr(128).chr(147) => '-', # N-Dash
    chr(226).chr(128).chr(148) => '-', # M-Dash 2
    chr(226).chr(128).chr(149) => '-', # M-Dash 3
    chr(226).chr(128).chr(152) => '\'', # Left Single Quote 1
    chr(226).chr(128).chr(153) => '\'', # Right Single Quote 1
    chr(226).chr(128).chr(156) => '"', # Left Double Quote 1
    chr(226).chr(128).chr(157) => '"', # Right Double Quote 1
    chr(226).chr(128).chr(162) => '*', # Bullet
    chr(226).chr(128).chr(166) => '...', # Ellipsis
    chr(226).chr(128).chr(178) => '\'', # Prime
    chr(226).chr(128).chr(179) => '"', # Capital Prime
    chr(226).chr(128).chr(185) => '<', # Left Soft Quote
    chr(226).chr(128).chr(186) => '>', # Right Soft Quote
    chr(226).chr(128).chr(190) => '-', # O-Line
    chr(226).chr(148).chr(128) => '-', # M-Dash 4
    chr(233) => 'e', # Latin e with acute
    chr(36) => '$', # Dollary Sign
    chr(92).chr(39) => '\'', # Right Single Quote 2
    chr(92).chr(39).chr(92).chr(39) => '"', # Right Double Quote 2
    chr(96) => '\'', # Left Single Quote 2
    chr(96).chr(96) => '"', # Left Double Quote 2
    chr(194) => ' ', # Caret over A
  );
  return strtr($text, $arr);
}
?> 